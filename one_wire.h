/*
* 
* Copyright (C) Patryk Jaworski <regalis@regalis.com.pl>
* 
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
*/

#ifndef __ONE_WIRE_H__
#define __ONE_WIRE_H__

#define ONE_WIRE_PIN B, 4

/** @brief Initialize one-wire pin
 *
 * Function will set one-wire pin as input
 * and disable internal pull-up resistor
 */
void one_wire_init();

/**
 * @brief Read 8 bits of data from the bus
 *
 * @return 8 bits of data
 */
uint8_t one_wire_read();

/**
 * @brief Reset one-wire bus by pulling
 * data line low for 480us
 */
void one_wire_reset_pulse();

/**
 * @brief Write single byte
 * @param data 8-bit data
 */
void one_wire_write(uint8_t data);


/**
 * @brief Read 64-bit ROM code
 * @return pointer to an array containg 
 * 8 bytes representing ROM code. LSB in
 * array[0]. Function will return 0 if
 * an error ocurred during communication
 */
uint8_t *one_wire_read_rom();

/**
 * @brief Address specific slave device
 * @param rom pointer to an array containg
 * 8 bytes of ROM. LSB must be placed at
 * rom[0].
 * @return 0 if operation was successfull
 * and value greater than 0 if there was
 * an error during communication
 */
uint8_t one_wire_match_rom(const uint8_t *rom);

/**
 * @brief Address all slave devices
 * @return 0 if operation was successfull
 * and value greater than 0 if there was
 * an error during communication
 */
uint8_t one_wire_skip_rom();

/** 
 * @brief Verify CRC of 64-bit rom stored
 * in array of 8 bytes.
 *
 * @param rom array containing 8 bytes of rom,
 * rom[0] should be device type identifier, rom[7]
 * should be CRC to check
 * @return 0 if CRC is correct, value greater than zero
 * if CRC is not correct
 */
uint8_t one_wire_verify_rom(const uint8_t *rom);

void one_wire_write_bit_0();
void one_wire_write_bit_1();
uint8_t one_wire_read_bit();

uint8_t one_wire_search_rom(uint8_t *rom, uint8_t last_deviation);
#define ONE_WIRE_SEARCH_FAILED 0xFF
#define ONE_WIRE_SEARCH_FINISHED 0x00


#endif

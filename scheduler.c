/*
* 
* Copyright (C) Patryk Jaworski <regalis@regalis.com.pl>
* 
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
*/

#ifdef DEBUG
#include <stdio.h>
#endif
#include <util/atomic.h>
#include "scheduler.h"

#ifndef SCHEDULER_MAX_TASKS
	#define SCHEDULER_MAX_TASKS 48
#endif

static task_t tasks[SCHEDULER_MAX_TASKS];
static volatile uint8_t tasks_size;

static inline void scheduler_up_heap() {
	if (tasks_size == 0)
		return;
	uint8_t current_index = tasks_size - 1;
	uint8_t parent = ((current_index - 1) >> 1);
	task_t tmp = tasks[current_index];
	while (current_index > 0 && tasks[parent].priority < tmp.priority) {
		tasks[current_index] = tasks[parent];
		current_index = parent;
		parent = ((current_index - 1) >> 1);
	}
	tasks[current_index] = tmp;
}

static inline void scheduler_down_heap() {
	uint8_t child;
	uint8_t current = 0;
	// save root
	task_t tmp = tasks[current];
	while (current < (tasks_size >> 1)) {
		// left child
		child = (current << 1) + 1;
		// maybe right child has greater priority
		if (child < (tasks_size - 1) &&
		tasks[child].priority < tasks[child + 1].priority) {
			child++;
		}
		if (tmp.priority >= tasks[child].priority) {
			break;
		}
		tasks[current] = tasks[child];
		current = child;
	}
	tasks[current] = tmp;
}

void scheduler_init() {
	tasks_size = 0;
}

uint8_t scheduler_add_task(task_t task) {
	uint8_t ret;
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		if (tasks_size == SCHEDULER_MAX_TASKS) {
			ret = SCHEDULER_QUEUE_FULL;
		} else {
			tasks[tasks_size++] = task;
			scheduler_up_heap();
			ret = tasks_size;
		}
	}
	return ret;
}

uint8_t scheduler_add_task_irq(task_t task) {
	if (tasks_size == SCHEDULER_MAX_TASKS) {
		return SCHEDULER_QUEUE_FULL;
	} else {
		tasks[tasks_size++] = task;
		scheduler_up_heap();
		return tasks_size;
	}
}

task_t scheduler_next_task() {
	task_t root;
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		root = tasks[0];
		tasks[0] = tasks[--tasks_size];
		scheduler_down_heap();
	}
	return root;
}

uint8_t scheduler_task_available() {
	return tasks_size != 0;
}

uint8_t scheduler_exec_task(const task_t *task) {
	return task->task_f(task->data);
}

#ifdef DEBUG
	void scheduler_dump() {
		int i = 0;
		while (i != tasks_size) {
			printf("[D][%.2d] %d\n", i, tasks[i].priority);
			++i;
		}
	}
#endif

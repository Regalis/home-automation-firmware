/*
* 
* Copyright (C) Patryk Jaworski <regalis@regalis.com.pl>
* 
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* Contributors:
*  -> Ariana Las
* 
*/

#ifndef __HELPFULL_H__
#define __HELPFULL_H__

#define STR(X) #X
#define DDR(X) DDR ## X
#define PORT(X) PORT ## X
#define GET_IN(X, Y) PIN ## X
#define GET_PIN(X, Y) Y
#define PIN(X) GET_PIN(X)
#define IN(X) GET_IN(X)

#define __PIN_LOW(X, Y) PORT(X) &= ~(_BV(Y))
#define __PIN_HIGH(X, Y) PORT(X) |= _BV(Y)
#define __PIN_TOGGLE(X, Y) PORT(X) ^= _BV(Y)
#define __PIN_SET_OUTPUT(X, Y) DDR(X) |= _BV(Y)
#define __PIN_SET_INPUT(X, Y) DDR(X) &= ~(_BV(Y))
#define __PIN_GET_VALUE(X, Y) (GET_IN(X, Y) & _BV(Y))
#define pin_low(X) __PIN_LOW(X)
#define pull_up_enable(X) __PIN_HIGH(X)
#define pull_up_disable(X) __PIN_LOW(X)
#define pin_high(X) __PIN_HIGH(X)
#define pin_toggle(X) __PIN_TOGGLE(X)
#define pin_set_output(X) __PIN_SET_OUTPUT(X)
#define pin_set_input(X) __PIN_SET_INPUT(X)
#define pin_get_value(X) __PIN_GET_VALUE(X)

#endif

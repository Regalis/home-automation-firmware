/*
* 
* Copyright (C) Patryk Jaworski <regalis@regalis.com.pl>
* 
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* Contributors:
*  -> Ariana Las
* 
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include "uart.h"

volatile static const char *buffer;

void uart_init() { // asynchronous
	UBRR0L = 25; // 38400 boud
	UCSR0A |= _BV(U2X0); // to elimit error
	UCSR0B |= _BV(TXEN0); // transmit only
	UCSR0C |= _BV(UCSZ01) | _BV(UCSZ00); // 8 bit
}

void uart_putc(const char x) {
	while (!(UCSR0A & _BV(UDRE0)));
	UDR0 = x;
}

void uart_puts(const char *x) {
	while (*x != '\0') {
		uart_putc(*x);
		x++;
	}
}

ISR(USART_TX_vect) {
	if (*buffer != '\0') {
		UDR0 = *(buffer++);
	} else {
		UCSR0B &= ~(_BV(TXCIE0));
	}
}

void uart_puts_async(const char *x) {
	buffer = x;
	UCSR0B |= _BV(TXCIE0);
}

void uart_wait_until_send_completed() {
	while (UCSR0B & _BV(TXCIE0));
}


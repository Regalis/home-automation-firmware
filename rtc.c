/*
* 
* Copyright (C) Ariana Las <ariana.las@gmail.com>
* 
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* Contributors:
*  -> Patryk Jaworski <regalis@regalis.com.pl>
* 
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include "rtc.h"

volatile rtc_date_time_t rtc;

void rtc_init() {
	ASSR |= _BV(AS2);
	TCNT2 = 0x00;
	TCCR2A = 0x00;
	TCCR2B = _BV(CS22) | _BV(CS20);
	while(ASSR & (_BV(TCN2UB) | _BV(TCR2AUB) | _BV(TCR2BUB)));
	TIFR2 = _BV(OCF2A) | _BV(OCF2B) | _BV(TOV2);
	TIMSK2 |= _BV(TOIE2);
}


ISR(TIMER2_OVF_vect) {
	TCCR2B = _BV(CS22) | _BV(CS20);
	if (++rtc.time.second == 60) {
		++rtc.time.minute;
		rtc.time.second = 0;
	}
	if (rtc.time.minute == 60) {
		++rtc.time.hour;
		rtc.time.minute = 0;
	}
	if (rtc.time.hour == 24) {
		++rtc.date.day;
		rtc.time.hour = 0;
	}
	if (rtc.date.day == 32) {
		++rtc.date.month;
		rtc.date.day = 1;
	} else if (rtc.date.day == 31 && (rtc.date.month == 4 || rtc.date.month == 6 || rtc.date.month == 9 || rtc.date.month == 11)) {
		++rtc.date.month;
		rtc.date.day = 1;
	} else if (rtc.date.month == 2 && rtc.date.day == 29) {
		if (rtc.date.year%100 == 0) {
			if (rtc.date.year%400 == 0) {
				++rtc.date.month;
				rtc.date.day = 1;
			} else {
				if (rtc.date.month == 2 && rtc.date.day == 30) {
					++rtc.date.month;
					rtc.date.day = 1;
				} else {
					return;
				}
			}
		} else if (rtc.date.year%4 == 0) {
			++rtc.date.month;
			rtc.date.day = 1;
		} else {
			if (rtc.date.month == 2 && rtc.date.day == 30) {
					++rtc.date.month;
					rtc.date.day = 1;
			} else {
				return;
			}
		}
	}
	if (rtc.date.month == 13) {
		++rtc.date.year;
		rtc.date.month = 1;
	}
	return;
}

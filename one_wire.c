/*
* 
* Copyright (C) Patryk Jaworski <regalis@regalis.com.pl>
* 
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
*/

#include <avr/io.h>
#include <util/delay.h>
#include <util/atomic.h>
#include <util/crc16.h>

#include "io_defs.h"
#include "one_wire.h"

/* one-wire commands */
#define ONE_WIRE_SEARCH_ROM 0xF0
#define ONE_WIRE_READ_ROM 0x33
#define ONE_WIRE_MATCH_ROM 0x55
#define ONE_WIRE_SKIP_ROM 0xCC
#define ONE_WIRE_ALARM_SEARCH 0xEC

/*
 * one-wire delays
 * according to Atmel's document AVR318
 * Rev. 2579A-AVR-09/04
 */
#define ONE_WIRE_DELAY_A 6
#define ONE_WIRE_DELAY_B 64
#define ONE_WIRE_DELAY_C 60
#define ONE_WIRE_DELAY_D 10
#define ONE_WIRE_DELAY_E 9
#define ONE_WIRE_DELAY_F 55
#define ONE_WIRE_DELAY_G 0
#define ONE_WIRE_DELAY_H 480
#define ONE_WIRE_DELAY_I 70
#define ONE_WIRE_DELAY_J 410

#define ONE_WIRE_BUS_LOW() pin_set_output(ONE_WIRE_PIN)
#define ONE_WIRE_BUS_HIGH() pin_set_input(ONE_WIRE_PIN)
#define ONE_WIRE_BUS_RELEASE() pin_set_input(ONE_WIRE_PIN)
#define ONE_WIRE_BUS_READ() pin_get_value(ONE_WIRE_PIN)


/* private functions */
static uint8_t one_wire_initialize();

/* function definitions */

void one_wire_write_bit_1() {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		ONE_WIRE_BUS_LOW();
		_delay_us(ONE_WIRE_DELAY_A);
		ONE_WIRE_BUS_RELEASE();
		_delay_us(ONE_WIRE_DELAY_B);
	}
}

void one_wire_write_bit_0() {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		ONE_WIRE_BUS_LOW();
		_delay_us(ONE_WIRE_DELAY_C);
		ONE_WIRE_BUS_RELEASE();
		_delay_us(ONE_WIRE_DELAY_D);
	}
}

uint8_t one_wire_read_bit() {
	uint8_t data  = 0x00;
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		ONE_WIRE_BUS_LOW();
		_delay_us(ONE_WIRE_DELAY_A);
		ONE_WIRE_BUS_RELEASE();
		_delay_us(ONE_WIRE_DELAY_E);
		data = ONE_WIRE_BUS_READ();
		_delay_us(ONE_WIRE_DELAY_F);
	}
	return data;
}

void one_wire_init() {
	pull_up_disable(ONE_WIRE_PIN);
	pin_set_input(ONE_WIRE_PIN);
}

void one_wire_reset_pulse() {
	ONE_WIRE_BUS_LOW();
	_delay_us(ONE_WIRE_DELAY_H);
	ONE_WIRE_BUS_RELEASE();
}

uint8_t one_wire_check_presence() {
	uint8_t data;
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		_delay_us(ONE_WIRE_DELAY_I);
		data = ONE_WIRE_BUS_READ();
		_delay_us(ONE_WIRE_DELAY_J);
	}
	return (data == 0x00);
}

void one_wire_write(uint8_t data) {
	for (uint8_t mask = 0x01; mask != 0; mask <<= 1) {
		if (data & mask) {
			one_wire_write_bit_1();
		} else {
			one_wire_write_bit_0();
		}
	}
}

uint8_t one_wire_read() {
	uint8_t data = 0;
	for (uint8_t i = 0; i < 8; ++i) {
		if (one_wire_read_bit())
			data |= _BV(i);
	}
	return data;
}

static uint8_t one_wire_initialize() {
	uint8_t presence;
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		one_wire_reset_pulse();
		presence = one_wire_check_presence();
	}
	return presence;
}

uint8_t *one_wire_read_rom() {
	static uint8_t rom[8];
	if (one_wire_initialize() == 0)
		return 0;
	one_wire_write(ONE_WIRE_READ_ROM);
	for (int8_t i = 0; i < 8; ++i) {
		rom[i] = one_wire_read();
	}
	return rom;
}

uint8_t one_wire_match_rom(const uint8_t *rom) {
	if (one_wire_initialize() == 0)
		return 1;
	one_wire_write(ONE_WIRE_MATCH_ROM);
	for (uint8_t i = 0; i < 8; ++i) {
		one_wire_write(rom[i]);
	}
	return 0;
}

uint8_t one_wire_skip_rom() {
	if (one_wire_initialize() == 0)
		return 1;
	one_wire_write(ONE_WIRE_SKIP_ROM);
	return 0;
}

uint8_t one_wire_verify_rom(const uint8_t *rom) {
	uint8_t crc = 0;
	for (uint8_t i = 0; i < 8; ++i) {
		crc = _crc_ibutton_update(crc, rom[i]);
	}
	return crc;
}

uint8_t one_wire_search_rom(uint8_t *rom, uint8_t last_deviation) {
	if (!one_wire_initialize())
		return ONE_WIRE_SEARCH_FAILED;
	uint8_t new_deviation = 0;
	uint8_t mask = 0x01;
	uint8_t bit_a;
	uint8_t bit_b;
	one_wire_write(ONE_WIRE_SEARCH_ROM);
	for (uint8_t i = 1; i <= 64; ++i) {
		bit_a = one_wire_read_bit();
		bit_b = one_wire_read_bit();
		if (bit_a && bit_b) {
			return ONE_WIRE_SEARCH_FAILED;
		} else if (bit_a ^ bit_b) {
			if (bit_a) {
				*rom |= mask;
			} else {
				*rom &= ~(mask);
			}
		} else {
			if (i == last_deviation) {
				*rom |= mask;
			} else if (i > last_deviation) {
				*rom &= ~(mask);
				new_deviation = i;
			} else if (!((*rom) & mask)) {
				new_deviation = i;
			}
		}

		if (*rom & mask) {
			one_wire_write_bit_1();
		} else {
			one_wire_write_bit_0();
		}

		mask <<= 1;
		if (!mask) {
			mask = 0x01;
			++rom;
		}
	}

	return new_deviation;
}

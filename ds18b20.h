/*
* 
* Copyright (C) Patryk Jaworski <regalis@regalis.com.pl>
* 
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* Contributors:
*  -> Ariana Las
* 
*/

#ifndef __DS18B20_H__
#define __DS18B20_H__

void ds18b20_start_conversion(const uint8_t *rom_code, uint8_t block);
uint16_t ds18b20_read_temperature(const uint8_t *rom_code);
int8_t ds18b20_get_decimal_temperature(uint16_t temp_raw);
uint16_t ds18b20_get_fractional_temperature();

#endif

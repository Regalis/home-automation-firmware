/*
* 
* Copyright (C) Patryk Jaworski <regalis@regalis.com.pl>
* 
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdlib.h>
#include <string.h>
#include "power.h"
#include "one_wire.h"
#include "rtc.h"
#include "uart.h"
#include "ds18b20.h"

uint16_t one_wire_errors = 0;

struct one_wire_device {
	uint8_t rom[8];
};

struct one_wire_device ow_devices[5];
uint8_t one_wire_devices_no = 0;

void one_wire_dump_rom(uint8_t *rom) {
	static char rom_buffer[4];
	for (uint8_t i = 0; i < 8; ++i) {
		uart_puts(itoa(rom[i], rom_buffer, 16));
	}
}

void one_wire_search_devices() {
	one_wire_devices_no = 0;
	char buffer[10];
	uart_puts("Searching for devices on One Wire bus...\n\r");
	memset(ow_devices[0].rom, 0, 8);
	uint8_t tmp_rom[8];
	uint8_t last_deviation = 0;
	while (1) {

		if (one_wire_devices_no != 0) {
			memcpy(tmp_rom, ow_devices[one_wire_devices_no - 1].rom, 8);
		}

		last_deviation = one_wire_search_rom(tmp_rom, last_deviation);
		memcpy(ow_devices[one_wire_devices_no++].rom, tmp_rom, 8);

		if (last_deviation == ONE_WIRE_SEARCH_FINISHED) {
			uart_puts("Found new device: ");
			one_wire_dump_rom(tmp_rom);
			uart_puts("\n\r");
			uart_puts("One wire search finished :-)\n\r");
			itoa(one_wire_devices_no, buffer, 10);
			uart_puts("One wire devices: ");
			uart_puts(buffer);
			uart_puts("\n\r");
			return;
		} else if (last_deviation == ONE_WIRE_SEARCH_FAILED) {
			uart_puts("One wire search failed! :-(\n\r");
			return;
		}

		uart_puts("Found new device: ");
		one_wire_dump_rom(tmp_rom);
		uart_puts("\n\r");
	}
}

int main() {
	power_init();
	uart_init();
	rtc_init();
	one_wire_init();
	sei();
	if (power_source() == POWER_SOURCE_AC) {
		uart_puts("Power source: AC\n\r");
	} else {
		uart_puts("Power source: Backup\n\r");
	}
	_delay_ms(1000);
	char d[10];
	while(1) {
		power_start_measure_backup_voltage();
		if (power_source_changed()) {
			uart_puts("Power changed to: ");
			if (power_source() == POWER_SOURCE_AC) {
				uart_puts("AC\n\r");
			} else {
				uart_puts("Backup\n\r");
			}
		}

		one_wire_search_devices();

		uint8_t *rom = one_wire_read_rom();
		if (rom == 0) {
			uart_puts("One wire presence failure\n\r");
		} else {
			uart_puts("One wire rom: ");
			for (int8_t i = 0; i < 8; ++i) {
				itoa(rom[i], d, 16);
				uart_puts(d);
			}
			if (one_wire_verify_rom(rom) != 0) {
				uart_puts(" (CRC ERROR)");
			} else {
				uart_puts(" (CRC OK)");
				ds18b20_start_conversion(rom, 1);
				uint16_t raw = ds18b20_read_temperature(rom);
				itoa(ds18b20_get_decimal_temperature(raw), d, 10);
				uart_puts("\n\r");
				uart_puts("Temp (rom): ");
				uart_puts(d);
				uart_puts("\n\r");
				ds18b20_start_conversion(0, 1);
				raw = ds18b20_read_temperature(0);
				itoa(ds18b20_get_decimal_temperature(raw), d, 10);
				uart_puts("\n\r");
				uart_puts("Temp (skip rom): ");
				uart_puts(d);
				uart_puts("\n\r");
			}
			uart_puts("\n\r");
		}

		itoa(power_get_backup_voltage(), d, 10);
		uart_puts("Battery voltage: ");
		uart_puts(d);
		uart_puts("mV\n\r");
		itoa(power_start_measure_backup_voltage_noise_reduction(), d, 10);
		uart_puts("\n\r   Battery voltage (noise reduction): ");
		uart_puts(d);
		uart_puts("mV\n\r\n\r");
		_delay_ms(500);
	}

}

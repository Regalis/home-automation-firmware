/*
* 
* Copyright (C) Ariana Las <ariana.las@gmail.com>
* 
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* Contributors:
*  -> Patryk Jaworski <regalis@regalis.com.pl>
* 
*/

#ifndef __RTC_H__
#define __RTC_H__
#include <stdint.h>

struct rtc_date {
	uint8_t day;
	uint8_t month;
	uint8_t year;
};

struct rtc_time {
	uint8_t hour;
	uint8_t minute;
	uint8_t second;
};

struct rtc_date_time {
	struct rtc_date date;
	struct rtc_time time;
};

typedef struct rtc_date rtc_date_t;
typedef struct rtc_time rtc_time_t;
typedef struct rtc_date_time rtc_date_time_t;

extern volatile rtc_date_time_t rtc;

void rtc_init();

#endif

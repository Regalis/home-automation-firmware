/*
* 
* Copyright (C) Patryk Jaworski <regalis@regalis.com.pl>
* 
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/atomic.h>
#include "hcsr501.h"
#include "rtc.h"
#include "io_defs.h"

volatile rtc_date_time_t last_movement;

void hcsr501_init() {
	// pin INT0(PD2) input
	pin_set_input(HCSR501_PIN);
	// interrupt rising edge 
	EICRA |= _BV(ISC01) | _BV(ISC00);
	// enable external interrupt on INT0
	EIMSK |= _BV(INT0);
}

void hcsr501_last_movement(rtc_date_time_t *dst) {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		*dst = last_movement;
	}
}

ISR(INT0_vect) {
	last_movement = rtc;
}

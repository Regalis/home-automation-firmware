/*
* 
* Copyright (C) Patryk Jaworski <regalis@regalis.com.pl>
* 
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
*/

#include <stdlib.h>
#include <string.h>
#include "string_utils.h"

static void help(uint8_t data, char *buffer) {
	if (data < 10) {
		*buffer = '0';
		*(buffer + 1) = data + '0';
	} else {
		*buffer = (data / 10) + '0';
		*(buffer + 1) = (data % 10) + '0';
	}
}

void time_to_string(const rtc_time_t *time, char *buffer) {
	help(time->hour, buffer);
	buffer += 2;
	*(buffer++) = ':';
	help(time->minute, buffer);
	buffer += 2;
	*(buffer++) = ':';
	help(time->second, buffer);
	*(buffer + 2) = '\0';
}

/*
char *date_to_string(rtc_date_t date) {

}

char *date_time_to_string(rtc_date_time_t date) {

}
*/

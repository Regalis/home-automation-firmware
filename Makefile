# 
# Copyright (C) Patryk Jaworski <regalis@regalis.com.pl>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# 

CLOCK=8000000UL

CC=avr-gcc
MCU=atmega328p
CFLAGS=-mmcu=$(MCU) -D F_CPU=$(CLOCK) -Os -Wall -Werror -std=gnu99 \
			-ffunction-sections -fdata-sections
LDFLAGS=-mmcu=$(MCU) -Wl,--gc-sections,--print-gc-sections
COM_SPEED=38400
COM_PORT=/dev/ttyUSB0
COM_DATA_BITS=8

SOURCES=$(wildcard *.c)
OBJS=$(patsubst %.c, %.o, $(SOURCES))

all: main.hex

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

main.elf: $(OBJS)
	$(CC) $(LDFLAGS) $^ -o $@

main.hex: main.elf
	avr-objcopy -R .eeprom -O ihex $< $@

clean:
	rm -rv *.o *.hex *.elf

write: main.hex
	avrdude -c usbasp -p $(MCU) -U flash:w:$<

serial:
	picocom -d $(COM_DATA_BITS) -b $(COM_SPEED) $(COM_PORT)

.PHONY: clean write all serial

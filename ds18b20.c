/*
* 
* Copyright (C) Patryk Jaworski <regalis@regalis.com.pl>
* 
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* Contributors:
*  -> Ariana Las
* 
*/

#include <avr/io.h>
#include <util/delay.h>
#include "one_wire.h"
#include "ds18b20.h"
#include "io_defs.h"

#define DS18B20_CONVERT_T 0x44
#define DS18B20_READ_SCRATCHPAD 0xBE
#define DS18B20_WRITE_SCRATCHPAD 0x4E
#define DS18B20_COPY_SCRATCHPAD 0x48
#define DS18B20_RECALL_E2 0xB8
#define DS18B20_READ_POWER_SUPPLY 0xB4

#define DS18B20_DECIMAL_MASK 0x07F0
#define DS18B20_FRACTIONAL_MASK 0x000F
#define DS18B20_SIGN_MASK 0x8000
#define DS18B20_DECIMAL_SHIFT 4

void ds18b20_start_conversion(const uint8_t *rom_code, uint8_t block) {
	if (rom_code == 0) {
		one_wire_skip_rom();
	} else {
		one_wire_match_rom(rom_code);
	}
	one_wire_write(DS18B20_CONVERT_T);
	if (block) {
		_delay_ms(800);
	}
}

uint16_t ds18b20_read_temperature(const uint8_t *rom_code) {
	if (rom_code == 0) {
		one_wire_skip_rom();
	} else {
		one_wire_match_rom(rom_code);
	}
	one_wire_write(DS18B20_READ_SCRATCHPAD);
	uint16_t temp_raw = one_wire_read();
	temp_raw |= (one_wire_read() << 8);
	one_wire_reset_pulse();
	return temp_raw;
}

int8_t ds18b20_get_decimal_temperature(uint16_t temp_raw) {
	int8_t temp = (int8_t)((temp_raw & DS18B20_DECIMAL_MASK) >> DS18B20_DECIMAL_SHIFT);
	if (temp_raw & DS18B20_SIGN_MASK) {
		temp *= -1;
	}
	return temp;
}

uint16_t ds18b20_get_fractional_temperature() {
	return 0;
}

/*
* 
* Copyright (C) Patryk Jaworski <regalis@regalis.com.pl>
* 
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include "power.h"

/* Maximum volatege at 1.1V (in mV) */
#define ADC_VOLTAGE_MAX 11864UL
#define ADC_REFERENCE_VOLATAGE 1100UL
#define ADC_SAMPLES_NO 128
#define ADC_SAMPLES_NO_SHIFT 7

static volatile uint8_t __power_source;
static volatile uint8_t __power_source_change;
static volatile uint32_t __power_ADC_sum;
static volatile uint16_t __power_ADC_ready;

ISR(ANALOG_COMP_vect) {
	__power_source = ACSR & _BV(ACO);
	__power_source_change = 1;
}

ISR(ADC_vect) {
	static uint32_t samples_value_sum;
	static uint8_t samples_no = ADC_SAMPLES_NO;
	uint16_t sample_value = ADCL >> 6;
	sample_value |= (uint16_t)ADCH << 2;
	samples_value_sum += sample_value;
	if (!(--samples_no)) {
		ADCSRA &= ~(_BV(ADATE));
		samples_no = ADC_SAMPLES_NO;
		__power_ADC_sum = samples_value_sum;
		samples_value_sum = 0;
		__power_ADC_ready = 0xFF;
	}
}

void power_init() {
	ACSR |= _BV(ACIE); // Analog Comparator interrupt enable
	DIDR1 |= _BV(AIN1D) | _BV(AIN0D);
	// Vref = 1.1V, ADC5, left adjustment
	ADMUX |= _BV(REFS0) | _BV(REFS1) | _BV(MUX0) | _BV(MUX2) | _BV(ADLAR);
	__power_source = ACSR & _BV(ACO);
	// ADC enable, interrupt enable, prescaler 128
	ADCSRA |= _BV(ADEN) | _BV(ADIE) | _BV(ADPS0) | _BV(ADPS1) | _BV(ADPS2);
}

uint8_t power_source() {
	return __power_source;
}

uint8_t power_source_changed() {
	if (__power_source_change == 1) {
		__power_source_change = 0;
		return 1;
	} else {
		return 0;
	}
}

void power_start_measure_backup_voltage() {
	ADCSRA |= _BV(ADATE);
	ADCSRA |= _BV(ADSC);
	__power_ADC_ready = 0;
}

uint16_t power_start_measure_backup_voltage_noise_reduction() {
	__power_ADC_ready = 0;
	while (!__power_ADC_ready) {
		set_sleep_mode(SLEEP_MODE_ADC);
		cli();
		sleep_enable();
		sei();
		sleep_cpu();
		sleep_disable();
	}
	return power_get_backup_voltage();
}

uint16_t power_get_backup_voltage() {
	while (!__power_ADC_ready);
	uint32_t voltage_raw =((ADC_REFERENCE_VOLATAGE * (__power_ADC_sum >> ADC_SAMPLES_NO_SHIFT)) >> 10);
	return (uint16_t)((voltage_raw * ADC_VOLTAGE_MAX) / ADC_REFERENCE_VOLATAGE);
}

/*
* 
* Copyright (C) Patryk Jaworski <regalis@regalis.com.pl>
* 
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
*/

#ifndef __SCHEDULER_H__
#define __SCHEDULER_H__
#include <stdint.h>

struct scheduler_task {
	uint8_t priority;
	uint8_t (*task_f)(void *data);
	void *data;
};

typedef struct scheduler_task task_t;

/**
 * @brief Remove all tasks
 */
void scheduler_init();

/**
 * @brief Insert new task
 *
 * @param task new task
 * @return number of tasks in queue or
 * SCHEDULER_QUEUE_FULL if queue is already full
 */
uint8_t scheduler_add_task(task_t task);
uint8_t scheduler_add_task_irq(task_t task);

/**
 * @breif Check if there is any task available
 * @return 0 if queue is empty, value greater than 0 is queue is not empty
 */
uint8_t scheduler_task_available();

/** 
 * @brief Pop (remove) next task from the queue
 * @return copy of removed task
 */
task_t scheduler_next_task();

/**
 * @brief Execute specific task
 * @param task to execute
 * @return value returned by task's function
 */
uint8_t scheduler_exec_task(const task_t *task);


#define SCHEDULER_QUEUE_FULL 0xFF

#ifdef DEBUG
	void scheduler_dump();
#endif

#endif
